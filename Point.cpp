#include "Point.h"
#include <cmath> 


Point::Point(double x, double y)
{
	_x = x;
	_y = y;
}

// copy consracteor 
Point::Point(const Point& other)
{
	this->_x = other.getX();
	this->_y = other.getY();
}

double Point::getX() const
{
	return _x;
}

double Point::getY() const
{
	return _y;
}


/*fonctiopn get  onther ponet and return a new thst includes the sums of the x  and thte y  of the two poins */
Point Point :: operator+(const Point& other) const
{
	Point p(0, 0);
	p._x -= other._x + this->_x;
	p._y -= other._y+ this->_y;
	return p;
}
/*fonction vhange the given point a and y to the sum of the x and y  of the two points*/
Point& Point:: operator+=(const Point& other)
{
	this->_x += other.getX();
	this->_y = other.getY();
	return *this;
}
// fonction return the distanc ebetween two points acor=ding to the formola 
double Point::distance(const Point& other) const
{
	return sqrt(pow(this->getX() - other.getX(), 2) +
		pow(this->getY() - other.getY(), 2) * 1.0);
}