#pragma once

#include "Shape.h"
#include "Point.h"

#define PI 3.14

class Circle : public Shape
{
public:
	Circle(const Point& center, double radius, const std::string& type, const std::string& name);
	~Circle();

	const Point& getCenter() const;
	double getRadius() const;

	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);

	// override functions if need (virtual + pure virtual)
	double getArea() const;
	double getPerimeter() const ;
private:
	Point _center;
	double _radius;
	std::string _name;
	std::string _type;
};