#pragma once
#include "Polygon.h"


namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	public:
		// There's a need only for the top left corner 
		Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name);
		virtual ~Rectangle();

		// override functions if need (virtual + pure virtual)
		double getPerimeter() const;
		double getArea() const;
	private: 
		Point _point;
		double _lentgh;
		double _length;
		double _width;
		std::string& _type;
		std::string& _name;

	};
}