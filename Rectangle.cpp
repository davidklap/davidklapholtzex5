#include "Rectangle.h"


myShapes::  Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name):
Polygon(type,name)
{
	_points.push_back(a);
	_points.push_back(a + Point(width, length));	
	_length = length;
	_width = width;
}


void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

double myShapes::Rectangle::getArea() const {
	return _length * _width;
}

double myShapes::Rectangle::getArea()const {

	return _length * 2 + _width * 2;
}


