	#include "Circle.h"

Circle ::Circle(const Point& center, double radius, const std::string& type, const std::string& name):
Shape(name,type),_center( center.getX(), center.getY())
{	
	_radius = radius;

}
void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

// retunr the area acording to th formola r^2 * pai
double Circle::getArea() const
{
	return _radius * _radius * PI; // acording to the formola 
}

double Circle::getPerimeter() const
{
	return 2 * getRadius() * PI;
}