#include "Arrow.h"


Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name):
	Shape(type,name),
	_pointA(a.getX(),a.getY()),
	_pointb(b.getX(), b.getY())
{
	
	_points.push_back(a);
	_points.push_back(b);
}

Arrow :: ~Arrow()
{

}


void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}


double Arrow::getArea() const
{
	return 0; // acording to the formola 
}
double Arrow::getPerimeter() const
{
	return _pointA.distance(_pointb);//return the distance between the two dotdse
}