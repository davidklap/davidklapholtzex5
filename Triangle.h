#pragma once
#include "Polygon.h"
#include "Polygon.h" 
#include <string>
#include "Point.h"
#include <iostream>
class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
	virtual ~Triangle();
	// override functions if need (virtual + pure virtual)
	double getArea() const;
	double getPerimeter() const;

private:
	Point pointA;
	Point pointB;
	Point pointC;
	std::string _type;
	std::string _name;
	int _high;
	int _width;
};

Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
