#include "Shape.h"
#include <iostream>

Shape::Shape(const std::string& name, const std::string& type)
{
	_name = name;
	_type = type;
}

/*fponction print all the detelis about the shape : name and type */
void Shape::printDetails() const
{
	std::cout << "shape nmae : " << _name << "\n";
	std::cout << "shape type : " << _type << "\n";
}


std::string Shape::getType() const
{
	return _type;
}


std::string Shape::getName() const
{
	return _name;
}


