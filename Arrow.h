#pragma once
#include "Polygon.h"

class Arrow : public Shape
{
public:
	Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name);
	~Arrow();

	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);

	// override functions if need (virtual + pure virtual)
	double getPerimeter() const;
	double getArea() const;
private:
	Point _pointA;
	Point _pointb;
	std::string _name;
	std::string _type;
	std::vector<Point> _points;
};